package id.kodesumsi.motiontrashure.adapter

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ItemNotificationBinding
import id.kodesumsi.motiontrashure.feature.notification.NotificationDetailActivity
import id.kodesumsi.motiontrashure.model.Notification

/**
 * Created by hilma on 11/11/2020.
 */
class NotificationAdapter(
    private val notifications: List<Notification>
): RecyclerView.Adapter<NotificationAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemNotificationBinding): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bindItem(notification: Notification) {
            Glide.with(context).load(notification.icon).into(binding.itemNotificationIcon)
            binding.itemNotificationTitle.text = notification.title
            binding.itemNotificationDesc.text = notification.desc
            binding.itemNotificationTime.text = notification.time
            if (notification.recent) {
                binding.itemNotificationRecent.visibility = View.VISIBLE
            } else {
                binding.itemNotificationRecent.visibility = View.GONE
            }

            binding.itemNotificationContainer.setOnClickListener {
                val toDetailNotification = Intent(context, NotificationDetailActivity::class.java)
                val bundleNotification = Bundle()
                bundleNotification.putString(NotificationDetailActivity.EXTRA_TITLE, notification.title)
                bundleNotification.putString(NotificationDetailActivity.EXTRA_DATE, notification.time)
                bundleNotification.putString(NotificationDetailActivity.EXTRA_DESC, notification.desc)
                toDetailNotification.putExtra(NotificationDetailActivity.EXTRA_NOTIFICATION, bundleNotification)
                context.startActivity(toDetailNotification)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemNotificationBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(notifications[position])
    }

    override fun getItemCount(): Int = notifications.size

}