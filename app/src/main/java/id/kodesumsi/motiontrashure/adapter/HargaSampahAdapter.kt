package id.kodesumsi.motiontrashure.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.kodesumsi.motiontrashure.databinding.ItemHargaSampahBinding
import id.kodesumsi.motiontrashure.model.HargaSampah
import id.kodesumsi.motiontrashure.utils.GeneralUtility.currencyFormat

/**
 * Created by hilma on 17/11/2020.
 */
class HargaSampahAdapter(
        private val items: List<HargaSampah>
): RecyclerView.Adapter<HargaSampahAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemHargaSampahBinding): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bindItem(item: HargaSampah) {
            Glide.with(context).load(item.thumb).into(binding.itemHargaSampahThumb)
            binding.itemHargaSampahJenis.text = item.jenisSampah
            binding.itemHargaSampahHarga.text = "Rp. ${currencyFormat(item.hargaSampah)}/kg"
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemHargaSampahBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int = items.size

}