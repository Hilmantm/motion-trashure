package id.kodesumsi.motiontrashure.adapter

import android.content.Intent
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextUtils.isEmpty
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ItemPenukaranBinding
import id.kodesumsi.motiontrashure.feature.penukaran.ChooseNominalActivity
import id.kodesumsi.motiontrashure.feature.penukaran.KonfirmasiPenukaranActivity
import id.kodesumsi.motiontrashure.model.Penukaran
import id.kodesumsi.motiontrashure.viewmodel.PenukaranViewModel

/**
 * Created by hilma on 18/11/2020.
 */
class PenukaranAdapter(
    private val items: List<Penukaran>,
    private val viewModel: PenukaranViewModel,
    private val observer: (holder: ViewHolder) -> Unit
): RecyclerView.Adapter<PenukaranAdapter.ViewHolder>() {

    class ViewHolder(
        val binding: ItemPenukaranBinding,
        private val viewModel: PenukaranViewModel
    ): RecyclerView.ViewHolder(binding.root) {

        val context = binding.root.context

        fun bindItem(item: Penukaran) {
            Glide.with(context).load(item.icon).into(binding.itemPenukaranIcon)
            binding.itemPenukaranName.text = item.title

            binding.itemPenukaranNominalEdittext.setOnClickListener {
                val toChooseNominalActivity = Intent(context, ChooseNominalActivity::class.java)
                context.startActivity(toChooseNominalActivity)
            }

            binding.itemPenukaranTukarBtn.setOnClickListener {
                val toKonfirmasiPenukaran = Intent(context, KonfirmasiPenukaranActivity::class.java)
                context.startActivity(toKonfirmasiPenukaran)
            }

            binding.itemPenukaranNoHpEdittext.addTextChangedListener(object: TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    Log.d("PenukaranAdapter", "beforeTextChanged() => " + s.toString())
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    Log.d("PenukaranAdapter", "onTextChanged() => " + s.toString())
                }

                override fun afterTextChanged(s: Editable?) {
                    Log.d("PenukaranAdapter", "afterTextChanged() => " + s.toString())
                    var providerDrawable = if(s.toString().isEmpty()) {
                        ContextCompat.getDrawable(context, R.drawable.ic_baseline_smartphone_24)!!
                    } else if(s.toString().contains("0812")) {
                        ContextCompat.getDrawable(context, R.drawable.kartu_telkomsel)!!
                    } else if(s.toString().contains("0896") || s.toString().contains("0897") || s.toString().contains("0898") || s.toString().contains("0899")) {
                        ContextCompat.getDrawable(context, R.drawable.kartu_tri)!!
                    } else {
                        ContextCompat.getDrawable(context, R.drawable.ic_baseline_smartphone_24)!!
                    }
                    viewModel.setPhoneIcon(providerDrawable)
                }
            })
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemPenukaranBinding.inflate(layoutInflater, parent, false), viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        observer(holder)
        holder.bindItem(items[position])

        holder.binding.itemPenukaranContainer.setOnClickListener {
            if(items[position].active) {
                items[position].expanded = !items[position].expanded
                notifyItemChanged(position)
            } else {
                Toast.makeText(holder.context, "Fitur belum tersedia", Toast.LENGTH_LONG).show()
            }
        }

        holder.binding.itemPenukaranExpandableContainer.layoutParams.height = if (items[position].expanded) LinearLayout.LayoutParams.WRAP_CONTENT else 0
        holder.binding.itemPenukaranChevron.rotation = if (items[position].expanded) 180f else 0f
    }

    override fun getItemCount(): Int = items.size
}