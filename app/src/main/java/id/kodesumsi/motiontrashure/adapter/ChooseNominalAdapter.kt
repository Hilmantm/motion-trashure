package id.kodesumsi.motiontrashure.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.kodesumsi.motiontrashure.databinding.ItemNominalBinding
import id.kodesumsi.motiontrashure.model.Nominal
import id.kodesumsi.motiontrashure.utils.GeneralUtility.currencyFormat
import id.kodesumsi.motiontrashure.viewmodel.PenukaranViewModel

/**
 * Created by hilma on 18/11/2020.
 */
class ChooseNominalAdapter(
    private val items: List<Nominal>,
    private val viewModel: PenukaranViewModel
): RecyclerView.Adapter<ChooseNominalAdapter.ViewHolder>() {

    class ViewHolder(
        private val binding: ItemNominalBinding,
        private val viewmodel: PenukaranViewModel
    ): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bindItem(item: Nominal) {
            binding.itemNominalHarga.text = currencyFormat(item.harga)
            binding.itemNominalNominal.text = "Rp. ${currencyFormat(item.nominal)}"

            binding.itemNominalContainer.setOnClickListener {
                viewmodel.setNominalState(item)
                (context as Activity).finish()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemNominalBinding.inflate(layoutInflater, parent, false), viewModel)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int = items.size
}