package id.kodesumsi.motiontrashure.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.kodesumsi.motiontrashure.databinding.ItemNotificationBinding
import id.kodesumsi.motiontrashure.databinding.ItemTipsBinding
import id.kodesumsi.motiontrashure.feature.beranda.TipsDetailActivity
import id.kodesumsi.motiontrashure.model.Tips

/**
 * Created by hilma on 14/11/2020.
 */
class TipsAdapter(
    private val tips: List<Tips>
): RecyclerView.Adapter<TipsAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemTipsBinding): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bindItem(item: Tips) {
            Glide.with(context).load(item.thumb).into(binding.itemTipsImage)
            binding.itemTipsTitle.text = item.title

            binding.itemTipsContainer.setOnClickListener {
                val toTipsDetail = Intent(context, TipsDetailActivity::class.java)
                context.startActivity(toTipsDetail)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemTipsBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(tips[position])
    }

    override fun getItemCount(): Int = tips.size
}