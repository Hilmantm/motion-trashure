package id.kodesumsi.motiontrashure.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ItemSetoranBinding
import id.kodesumsi.motiontrashure.model.Setoran

/**
 * Created by hilma on 12/11/2020.
 */
class SetoranAdapter(
    private val items: List<Setoran>
): RecyclerView.Adapter<SetoranAdapter.ViewHolder>() {

    class ViewHolder(private val binding: ItemSetoranBinding): RecyclerView.ViewHolder(binding.root) {

        private val context = binding.root.context

        fun bindItem(setoran: Setoran) {
            binding.itemSetoranTrashbagId.text = "Trashbag ID ${setoran.trashBagId}}"
            Glide.with(context).load(setoran.icon).into(binding.itemSetoranTrashImage)
            binding.itemSetoranDate.text = setoran.date
            if (setoran.status == 0) {
                binding.itemSetoranProsesStatus.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.setoranProsesColor)))
                binding.itemSetoranProsesStatus.text = "Proses"
            } else {
                binding.itemSetoranProsesStatus.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(context, R.color.colorPrimary)))
                binding.itemSetoranProsesStatus.text = "Selesai"
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(ItemSetoranBinding.inflate(layoutInflater, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItem(items[position])
    }

    override fun getItemCount(): Int = items.size
}