package id.kodesumsi.motiontrashure.model

import android.graphics.drawable.Drawable

/**
 * Created by hilma on 17/11/2020.
 */
data class HargaSampah(

        val id: Int,

        val jenisSampah: String,

        val hargaSampah: Int,

        val thumb: Drawable

)