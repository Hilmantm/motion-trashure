package id.kodesumsi.motiontrashure.model

import android.graphics.drawable.Drawable

/**
 * Created by hilma on 18/11/2020.
 */
data class Penukaran(

    val id: Int,

    val icon: Drawable,

    val title: String,

    var expanded: Boolean,

    val active: Boolean

)