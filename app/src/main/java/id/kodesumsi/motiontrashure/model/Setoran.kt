package id.kodesumsi.motiontrashure.model

import android.graphics.drawable.Drawable

/**
 * Created by hilma on 12/11/2020.
 */
data class Setoran(

    val id: Int,

    val icon: Drawable,

    val trashBagId: String,

    val date: String,

    // 0 = Proses
    // 1 = Selesai
    val status: Int

)