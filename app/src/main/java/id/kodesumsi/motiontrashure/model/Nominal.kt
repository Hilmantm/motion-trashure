package id.kodesumsi.motiontrashure.model

/**
 * Created by hilma on 18/11/2020.
 */
data class Nominal(

    val id: Int,

    val nominal: Int,

    val harga: Int

)