package id.kodesumsi.motiontrashure.model

import android.graphics.drawable.Drawable

/**
 * Created by hilma on 11/11/2020.
 */
data class Notification(

    val id: Int,

    val title: String,

    val desc: String,

    val time: String,

    val recent: Boolean,

    val icon: Drawable

)