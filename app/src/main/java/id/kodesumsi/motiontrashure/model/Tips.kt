package id.kodesumsi.motiontrashure.model

import android.graphics.drawable.Drawable

/**
 * Created by hilma on 14/11/2020.
 */
data class Tips(

    val id: Int,

    val title: String,

    val desc: String,
    
    val date : String,

    val thumb: Drawable

)