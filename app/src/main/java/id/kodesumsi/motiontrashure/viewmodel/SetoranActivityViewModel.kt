package id.kodesumsi.motiontrashure.viewmodel

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.model.Setoran

/**
 * Created by hilma on 05/12/2020.
 */
class SetoranActivityViewModel @ViewModelInject constructor(): ViewModel() {

    private val setoran: MutableLiveData<List<Setoran>> = MutableLiveData()

    fun setSetoran(length: Int, context: Context) {
        val setorans: MutableList<Setoran> = mutableListOf()
        for (i in 1..length) {
            val setoran = Setoran(
                id = i,
                icon = ContextCompat.getDrawable(context, R.drawable.ic_baseline_delete_24)!!,
                trashBagId = "1020309940",
                date = "19 / 04 / 2020",
                status = if (i == 1) 0 else 1
            )
            setorans.add(setoran)
        }
        setoran.postValue(setorans)
    }

    fun getSetoran(): LiveData<List<Setoran>> = setoran

}