package id.kodesumsi.motiontrashure.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.kodesumsi.motiontrashure.repository.NetworkRepository

/**
 * Created by hilma on 11/11/2020.
 */
class NotificationViewModel @ViewModelInject constructor(
    private val networkRepository: NetworkRepository
): ViewModel() {

    private val index: MutableLiveData<Int> = MutableLiveData()

    fun setIndex(value: Int) {
        index.postValue(value)
    }

    fun getIndex(): LiveData<Int> = index

}