package id.kodesumsi.motiontrashure.viewmodel

import android.graphics.drawable.Drawable
import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import id.kodesumsi.motiontrashure.model.Nominal
import id.kodesumsi.motiontrashure.repository.NetworkRepository

/**
 * Created by hilma on 18/11/2020.
 */
class PenukaranViewModel @ViewModelInject constructor(
    private val networkRepository: NetworkRepository
)  : ViewModel() {

    private val providerIcon: MutableLiveData<Drawable> = MutableLiveData()
    private val nominalState: MutableLiveData<Nominal> = MutableLiveData()

    fun setNominalState(nominal: Nominal) {
        nominalState.postValue(nominal)
    }

    fun getNominalState(): LiveData<Nominal> = nominalState

    fun setPhoneIcon(drawable: Drawable) {
        providerIcon.postValue(drawable)
    }

    fun getProviderIcon(): LiveData<Drawable> = providerIcon

}