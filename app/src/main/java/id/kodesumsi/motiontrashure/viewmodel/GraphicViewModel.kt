package id.kodesumsi.motiontrashure.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.github.mikephil.charting.data.BarEntry

/**
 * Created by hilma on 27/11/2020.
 */
class GraphicViewModel @ViewModelInject constructor(): ViewModel() {

    private val index: MutableLiveData<Int> = MutableLiveData()
    private val monthData: MutableLiveData<List<BarEntry>> = MutableLiveData()
    private val weekData: MutableLiveData<List<BarEntry>> = MutableLiveData()
    private val yearData: MutableLiveData<List<BarEntry>> = MutableLiveData()

    fun setIndex(value: Int) {
        index.postValue(value)
    }

    fun setMonthGraphicData() {
        val listOfBarEntry: MutableList<BarEntry> = mutableListOf()
        listOfBarEntry.add(BarEntry(0f, 10f))
        listOfBarEntry.add(BarEntry(1f, 15f))
        listOfBarEntry.add(BarEntry(2f, 5f))
        listOfBarEntry.add(BarEntry(3f, 18f))
        listOfBarEntry.add(BarEntry(4f, 25f))
        listOfBarEntry.add(BarEntry(5f, 30f))
        listOfBarEntry.add(BarEntry(6f, 35f))
        listOfBarEntry.add(BarEntry(7f, 22f))
        listOfBarEntry.add(BarEntry(8f, 32f))
        listOfBarEntry.add(BarEntry(9f, 14f))
        listOfBarEntry.add(BarEntry(10f, 34f))
        listOfBarEntry.add(BarEntry(11f, 40f))
        monthData.postValue(listOfBarEntry)
    }

    fun setWeekGraphicData() {
        val listOfBarEntry: MutableList<BarEntry> = mutableListOf()
        listOfBarEntry.add(BarEntry(0f, 5f))
        listOfBarEntry.add(BarEntry(1f, 10f))
        listOfBarEntry.add(BarEntry(2f, 3f))
        listOfBarEntry.add(BarEntry(3f, 13f))
        listOfBarEntry.add(BarEntry(4f, 0f))
        listOfBarEntry.add(BarEntry(5f, 0f))
        listOfBarEntry.add(BarEntry(6f, 0f))
        weekData.postValue(listOfBarEntry)
    }

    fun setYearGraphicData() {
        val listOfBarEntry: MutableList<BarEntry> = mutableListOf()
        listOfBarEntry.add(BarEntry(0f, 55f))
        listOfBarEntry.add(BarEntry(1f, 0f))
        listOfBarEntry.add(BarEntry(2f, 0f))
        listOfBarEntry.add(BarEntry(3f, 0f))
        yearData.postValue(listOfBarEntry)
    }

    fun getIndex(): LiveData<Int> = index

    fun getMonthGraphicData(): LiveData<List<BarEntry>> = monthData

    fun getWeekGraphicData(): LiveData<List<BarEntry>> = weekData

    fun getYearGraphicData(): LiveData<List<BarEntry>> = yearData

}