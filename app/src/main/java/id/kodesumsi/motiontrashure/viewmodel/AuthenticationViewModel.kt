package id.kodesumsi.motiontrashure.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * Created by hilma on 05/11/2020.
 */
class AuthenticationViewModel: ViewModel() {

    private val seePassword: MutableLiveData<Boolean> = MutableLiveData()

    fun seePasswordState(boolean: Boolean) {
        seePassword.postValue(boolean)
    }

    fun getSeePasswordState(): LiveData<Boolean> = seePassword

}