package id.kodesumsi.motiontrashure.utils

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import java.text.DecimalFormat
import java.text.NumberFormat

/**
 * Created by hilma on 26/10/2020.
 */
object GeneralUtility {

    fun changeTintColor(ctx: Context, color: Int): ColorStateList {
        return ColorStateList.valueOf(ContextCompat.getColor(ctx, color))
    }

    fun currencyFormat(price: Int): String {
        val formatter: NumberFormat = DecimalFormat("#.###")
        return formatter.format(price)
    }

}