package id.kodesumsi.motiontrashure.utils

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import java.util.*

/**
 * Created by hilma on 11/11/2020.
 */
class AxisFormatter(private val format: Int): ValueFormatter() {


    companion object {
        const val DAYS = 0
        const val MONTH = 1
        const val YEAR = 2
    }

    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
        var data: MutableList<String> = mutableListOf()
        when(format) {
            DAYS -> data.addAll(getDaysList())
            MONTH -> data.addAll(getMonthList())
            YEAR -> data.addAll(getYearList())
        }
        return data.getOrNull(value.toInt()) ?: value.toString()
    }

    private fun getMonthList(): List<String> {
        val months: MutableList<String> = mutableListOf()
        months.add("Jan")
        months.add("Feb")
        months.add("Mar")
        months.add("Apr")
        months.add("Mei")
        months.add("Jun")
        months.add("Jul")
        months.add("Aug")
        months.add("Sep")
        months.add("Okt")
        months.add("Nov")
        months.add("Des")
        return months
    }

    private fun getDaysList(): List<String> {
        val days: MutableList<String> = mutableListOf()
        days.add("Senin")
        days.add("Selasa")
        days.add("Rabu")
        days.add("Kamis")
        days.add("Jumat")
        days.add("Sabtu")
        days.add("Minggu")
        return days
    }

    private fun getYearList(): List<String> {
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val year: MutableList<String> = mutableListOf()
        year.add((currentYear).toString())
        year.add((currentYear + 1).toString())
        year.add((currentYear + 2).toString())
        year.add((currentYear + 4).toString())
        return year
    }

}