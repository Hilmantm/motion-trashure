package id.kodesumsi.motiontrashure.feature.penukaran

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.PenukaranAdapter
import id.kodesumsi.motiontrashure.databinding.FragmentPenukaranBinding
import id.kodesumsi.motiontrashure.model.Nominal
import id.kodesumsi.motiontrashure.model.Penukaran
import id.kodesumsi.motiontrashure.utils.GeneralUtility.currencyFormat
import id.kodesumsi.motiontrashure.viewmodel.PenukaranViewModel

@AndroidEntryPoint
class PenukaranFragment : Fragment(), NominalCallback {

    private lateinit var binding: FragmentPenukaranBinding

    private lateinit var penukaranAdapter: PenukaranAdapter

    private val penukaranViewModel: PenukaranViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPenukaranBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        penukaranAdapter = PenukaranAdapter(initDummyPenukaranData(), penukaranViewModel) { holder ->
            penukaranViewModel.getProviderIcon().observe(viewLifecycleOwner, Observer {
                Glide.with(holder.context).load(it).into(holder.binding.itemPenukaranRightNoHpDrawable)
            })

            penukaranViewModel.getNominalState().observe(viewLifecycleOwner, {
                Log.d("PenukarangFragment", "NominalState = $it")
                holder.binding.itemPenukaranNominalEdittext.setText("Rp. ${currencyFormat(it.nominal)}")
                holder.binding.itemPenukaranHarga.setText("Rp. ${currencyFormat(it.harga)}")
            })
        }
        binding.fragmentPenukaranToolbar.title = "Penukaran"
        binding.fragmentPenukaranRecyclerview.layoutManager = LinearLayoutManager(context)
        binding.fragmentPenukaranRecyclerview.adapter = penukaranAdapter
    }

    private fun initDummyPenukaranData(): List<Penukaran> {
        val listOfPenukaran: MutableList<Penukaran> = mutableListOf()

        val penukaranPulsa = Penukaran(
            id = 1,
            title = "Pulsa",
            expanded = false,
            active = true,
            icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_smartphone_24)!!
        )
        listOfPenukaran.add(penukaranPulsa)

        val penukaranDana = Penukaran(
            id = 2,
            title = "Dana",
            expanded = false,
            active = false,
            icon = ContextCompat.getDrawable(requireContext(), R.drawable.dana)!!
        )
        listOfPenukaran.add(penukaranDana)

        val penukaranLinkAja = Penukaran(
            id = 3,
            title = "Link Aja",
            expanded = false,
            active = false,
            icon = ContextCompat.getDrawable(requireContext(), R.drawable.link_aja)!!
        )
        listOfPenukaran.add(penukaranLinkAja)

        return listOfPenukaran
    }

    override fun setNominal(nominal: Nominal) {
        TODO("Not yet implemented")
    }
}