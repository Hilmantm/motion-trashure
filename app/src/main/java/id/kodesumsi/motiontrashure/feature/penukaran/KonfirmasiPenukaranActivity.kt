package id.kodesumsi.motiontrashure.feature.penukaran

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ActivityKonfirmasiPenukaranBinding

class KonfirmasiPenukaranActivity : AppCompatActivity() {

    private lateinit var binding: ActivityKonfirmasiPenukaranBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKonfirmasiPenukaranBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.activityKonfirmasiPenukaranToolbar)
        supportActionBar?.title = "Konfirmasi"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.activityKonfirmasiPenukaranTukarBtn.setOnClickListener {
            val toSuccessActivity = Intent(this, PenukaranSuccessActivity::class.java)
            startActivity(toSuccessActivity)
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}