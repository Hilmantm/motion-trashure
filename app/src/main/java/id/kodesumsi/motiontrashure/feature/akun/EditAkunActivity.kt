package id.kodesumsi.motiontrashure.feature.akun

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ActivityEditAkunBinding

class EditAkunActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditAkunBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditAkunBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.activityEditAkunToolbar)
        supportActionBar?.title = getString(R.string.title_activity_edit_akun)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}