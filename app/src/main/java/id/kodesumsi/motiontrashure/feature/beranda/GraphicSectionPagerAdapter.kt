package id.kodesumsi.motiontrashure.feature.beranda

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.kodesumsi.motiontrashure.R

/**
 * Created by hilma on 26/11/2020.
 */

private val TAB_TITLES = arrayOf(
    R.string.tab_graphic_1,
    R.string.tab_graphic_2,
    R.string.tab_graphic_3
)

class GraphicSectionPagerAdapter(
    private val context: Context, fm: FragmentManager
): FragmentPagerAdapter(fm) {

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment {
        return GraphicChildFragment.newInstance(position)
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return context.resources.getString(TAB_TITLES[position])
    }
}