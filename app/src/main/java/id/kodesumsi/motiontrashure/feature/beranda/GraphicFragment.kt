package id.kodesumsi.motiontrashure.feature.beranda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentGraphicBinding
import id.kodesumsi.motiontrashure.feature.notification.NotificationFragment

@AndroidEntryPoint
class GraphicFragment : Fragment() {

    private lateinit var binding: FragmentGraphicBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentGraphicBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fragmentGraphicToolbar.title = "Sampah Terkumpul"
        binding.fragmentGraphicViewPager.adapter = GraphicSectionPagerAdapter(requireContext(), childFragmentManager)
        binding.fragmentGraphicTabs.setupWithViewPager(binding.fragmentGraphicViewPager)
    }

}