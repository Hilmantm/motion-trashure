package id.kodesumsi.motiontrashure.feature.notification

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ActivityNotificationDetailBinding
import id.kodesumsi.motiontrashure.viewmodel.NotificationViewModel

@AndroidEntryPoint
class NotificationDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNotificationDetailBinding

    companion object {
        const val EXTRA_TITLE = "NOTIFICATION_EXTRA_TITLE"
        const val EXTRA_DATE = "NOTIFICATION_EXTRA_DATE"
        const val EXTRA_DESC = "NOTIFICATION_EXTRA_DESC"
        const val EXTRA_NOTIFICATION = "NOTIFICATION_EXTRA"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent = intent
        val extraNotification = intent.getBundleExtra(EXTRA_NOTIFICATION)

        setSupportActionBar(binding.activityNotificationDetailToolbar)
        supportActionBar?.title = extraNotification.getString(EXTRA_TITLE).toString()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.activityNotificationDetailTitle.text = extraNotification.getString(EXTRA_TITLE).toString()
        binding.activityNotificationDetailDate.text = extraNotification.getString(EXTRA_DATE).toString()
        binding.activityNotificationDetailDesc.text = extraNotification.getString(EXTRA_DESC).toString()

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}