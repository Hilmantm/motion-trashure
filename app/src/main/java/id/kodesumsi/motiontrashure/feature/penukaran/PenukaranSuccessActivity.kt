package id.kodesumsi.motiontrashure.feature.penukaran

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ActivityPenukaranSuccessBinding

class PenukaranSuccessActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPenukaranSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPenukaranSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.activityPenukaranSuccessBackBtn.setOnClickListener {
            onBackPressed()
            finish()
        }

    }
}