package id.kodesumsi.motiontrashure.feature.beranda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.SetoranAdapter
import id.kodesumsi.motiontrashure.databinding.ActivitySetoranBinding
import id.kodesumsi.motiontrashure.viewmodel.SetoranActivityViewModel

@AndroidEntryPoint
class SetoranActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySetoranBinding
    private lateinit var setoranAdapter: SetoranAdapter

    private val setoranActivityViewModel: SetoranActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySetoranBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.activitySetoranToolbar)
        supportActionBar?.title = getString(R.string.title_activity_setoran)
        val displayHomeAsUpEnabled = supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.activitySetoranRecyclerview.layoutManager = LinearLayoutManager(this)

        setoranActivityViewModel.setSetoran(20, this)
        observeViewModel()
    }

    private fun observeViewModel() {
        setoranActivityViewModel.getSetoran().observe(this, {
            setoranAdapter = SetoranAdapter(it)
            binding.activitySetoranRecyclerview.adapter = setoranAdapter
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}