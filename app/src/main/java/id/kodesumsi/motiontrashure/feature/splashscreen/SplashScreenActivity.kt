package id.kodesumsi.motiontrashure.feature.splashscreen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.feature.authentication.AuthenticationActivity
import id.kodesumsi.motiontrashure.utils.Constants.SPLASHSCREEN_DELAY

class SplashScreenActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            val toLoginActivity = Intent(this, AuthenticationActivity::class.java)
            startActivity(toLoginActivity)
            finish()
        }, SPLASHSCREEN_DELAY)

    }

}