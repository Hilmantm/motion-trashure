package id.kodesumsi.motiontrashure.feature.beranda

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentGraphicChildBinding
import id.kodesumsi.motiontrashure.utils.AxisFormatter
import id.kodesumsi.motiontrashure.viewmodel.GraphicViewModel

@AndroidEntryPoint
class GraphicChildFragment : Fragment() {

    private lateinit var binding: FragmentGraphicChildBinding
    private val graphicViewModel: GraphicViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        graphicViewModel.setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentGraphicChildBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observeViewModel()
    }

    private fun observeViewModel() {
        graphicViewModel.getIndex().observe(viewLifecycleOwner, {
            when(it) {
                AxisFormatter.DAYS -> {
                    graphicViewModel.setWeekGraphicData()
                    graphicViewModel.getWeekGraphicData().observe(viewLifecycleOwner, {
                        setGraphicData(it, AxisFormatter.DAYS)
                    })
                }
                AxisFormatter.MONTH -> {
                    graphicViewModel.setMonthGraphicData()
                    graphicViewModel.getMonthGraphicData().observe(viewLifecycleOwner, {
                        setGraphicData(it, AxisFormatter.MONTH)
                    })
                }
                AxisFormatter.YEAR -> {
                    graphicViewModel.setYearGraphicData()
                    graphicViewModel.getYearGraphicData().observe(viewLifecycleOwner, {
                        setGraphicData(it, AxisFormatter.YEAR)
                    })
                }
            }
        })
    }

    private fun setGraphicData(listOfBarEntry: List<BarEntry>, formatData: Int) {
        val xAxis = binding.fragmentGraphicChildChart.xAxis
        xAxis.valueFormatter = AxisFormatter(formatData)

        val barDataSet = BarDataSet(listOfBarEntry, "Sampah Terkumpul")
        val barData = BarData(barDataSet)
        binding.fragmentGraphicChildChart.animateY(300)
        binding.fragmentGraphicChildChart.offsetLeftAndRight(10)
        binding.fragmentGraphicChildChart.data = barData
    }

    companion object {

        private const val ARG_SECTION_NUMBER = "section_number"

        @JvmStatic
        fun newInstance(sectionNumber: Int) =
            GraphicChildFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
    }
}