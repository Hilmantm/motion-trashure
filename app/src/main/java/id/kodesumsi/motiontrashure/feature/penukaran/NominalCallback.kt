package id.kodesumsi.motiontrashure.feature.penukaran

import id.kodesumsi.motiontrashure.model.Nominal

/**
 * Created by hilma on 19/11/2020.
 */
interface NominalCallback {
    fun setNominal(nominal: Nominal)
}