package id.kodesumsi.motiontrashure.feature.akun

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.navigation.ui.AppBarConfiguration
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentAkunBinding

class AkunFragment : Fragment() {

    private lateinit var binding: FragmentAkunBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAkunBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fragmentAkunToolbar.title = getString(R.string.akun)
        binding.componentAkunProfileNameLayout.fragmentAkunProfileEditBtn.setOnClickListener {
            val toEditAkunActivity = Intent(context, EditAkunActivity::class.java)
            startActivity(toEditAkunActivity)
        }
    }

}