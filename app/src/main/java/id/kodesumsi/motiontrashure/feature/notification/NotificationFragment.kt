package id.kodesumsi.motiontrashure.feature.notification

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.NotificationAdapter
import id.kodesumsi.motiontrashure.databinding.FragmentNotificationBinding
import id.kodesumsi.motiontrashure.model.Notification
import id.kodesumsi.motiontrashure.viewmodel.NotificationViewModel

/**
 * A placeholder fragment containing a simple view.
 */
@AndroidEntryPoint
class NotificationFragment : Fragment() {

    private val notificationViewModel: NotificationViewModel by viewModels()

    private lateinit var binding: FragmentNotificationBinding

    private lateinit var notificationAdapter: NotificationAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        notificationViewModel.setIndex(arguments?.getInt(ARG_SECTION_NUMBER) ?: 0)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNotificationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.recyclerView.layoutManager = LinearLayoutManager(context)
        observeLiveData()
    }

    private fun observeLiveData() {
        notificationViewModel.getIndex().observe(viewLifecycleOwner, Observer {
            Log.d(NotificationFragment::class.java.simpleName, "Recent Page $it")
            val notifications: MutableList<Notification> = mutableListOf()
            if (it == 0) {
                notifications.addAll(initNotificationData())
            } else {
                notifications.addAll(initNotificationUpdateData())
            }
            notificationAdapter = NotificationAdapter(notifications)
            binding.recyclerView.adapter = notificationAdapter
        })
    }

    private fun initNotificationData(): List<Notification> {
        val notifications: MutableList<Notification> = mutableListOf()
        for (i in 1..5) {
            val notification = Notification(
                id = i,
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_smartphone_24)!!,
                title = "Penukaran Pulsa",
                desc = "Pulsa sebesar 5000 dengan nomor kartu Tri ...",
                time = "20.50",
                recent = false
            )
            notifications.add(notification)
        }
        return notifications
    }

    private fun initNotificationUpdateData(): List<Notification> {
        val notifications: MutableList<Notification> = mutableListOf()
        for (i in 1..3) {
            val notification = Notification(
                id = i,
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_trashure_logo_white_foreground)!!,
                title = "Update Trashure v1.${i}",
                desc = "Update App Trashure v1.3 kali ini merupakan ...",
                time = "20.50",
                recent = false
            )
            notifications.add(notification)
        }
        return notifications
    }

    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val ARG_SECTION_NUMBER = "section_number"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(sectionNumber: Int): NotificationFragment {
            return NotificationFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_SECTION_NUMBER, sectionNumber)
                }
            }
        }
    }

}