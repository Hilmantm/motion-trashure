package id.kodesumsi.motiontrashure.feature.authentication

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentRegisterBinding
import id.kodesumsi.motiontrashure.utils.GeneralUtility.changeTintColor
import id.kodesumsi.motiontrashure.viewmodel.AuthenticationViewModel

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    private lateinit var binding: FragmentRegisterBinding

    private val authenticationViewModel: AuthenticationViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fragmentRegisterButtonBack.setOnClickListener {
            activity?.onBackPressed()
        }

        binding.fragmentRegisterButtonMasuk.setOnClickListener {
            activity?.onBackPressed()
        }

        authenticationViewModel.seePasswordState(false)
        binding.fragmentRegisterFieldPasswordSeeIcon.setOnClickListener {
            authenticationViewModel.seePasswordState(!authenticationViewModel.getSeePasswordState().value!!)
        }

        observeLiveData()
    }

    private fun observeLiveData() {
        authenticationViewModel.getSeePasswordState().observe(viewLifecycleOwner) {
            if (it) {
                binding.fragmentRegisterFieldPasswordSeeIcon.imageTintList = changeTintColor(requireContext(), R.color.colorPrimary)
                binding.fragmentRegisterFieldPasswordEdittext.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                binding.fragmentRegisterFieldPasswordSeeIcon.imageTintList = changeTintColor(requireContext(), R.color.colorAuthenticationGrey)
                binding.fragmentRegisterFieldPasswordEdittext.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            binding.fragmentRegisterFieldPasswordEdittext.setSelection(binding.fragmentRegisterFieldPasswordEdittext.text.length)
        }
    }

}