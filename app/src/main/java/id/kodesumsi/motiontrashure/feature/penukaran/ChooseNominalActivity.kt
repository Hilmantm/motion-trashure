package id.kodesumsi.motiontrashure.feature.penukaran

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.ChooseNominalAdapter
import id.kodesumsi.motiontrashure.databinding.ActivityChooseNominalBinding
import id.kodesumsi.motiontrashure.model.Nominal
import id.kodesumsi.motiontrashure.viewmodel.PenukaranViewModel

@AndroidEntryPoint
class ChooseNominalActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChooseNominalBinding

    private val penukaranViewModel: PenukaranViewModel by viewModels()

    private lateinit var chooseNominalAdapter: ChooseNominalAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChooseNominalBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.activityChooseNominalToolbar)
        supportActionBar?.title = "Nominal"
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_baseline_close_24)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        chooseNominalAdapter = ChooseNominalAdapter(initDummyChooseNominal(), penukaranViewModel)
        binding.activityChooseNominalRecyclerview.layoutManager = LinearLayoutManager(this)
        binding.activityChooseNominalRecyclerview.adapter = chooseNominalAdapter
    }

    private fun initDummyChooseNominal(): List<Nominal> {
        val listOfNominal: MutableList<Nominal> = mutableListOf()

        var data = Nominal(
            id = 1,
            nominal = 1000,
            harga = 1500
        )
        listOfNominal.add(data)

        data = Nominal(
            id = 2,
            nominal = 5000,
            harga = 6000
        )
        listOfNominal.add(data)

        data = Nominal(
            id = 3,
            nominal = 10000,
            harga = 11000
        )
        listOfNominal.add(data)

        data = Nominal(
            id = 4,
            nominal = 20000,
            harga = 21000
        )
        listOfNominal.add(data)

        data = Nominal(
            id = 5,
            nominal = 50000,
            harga = 51000
        )
        listOfNominal.add(data)

        return listOfNominal
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}