package id.kodesumsi.motiontrashure.feature.beranda

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.kodesumsi.motiontrashure.databinding.ActivityTipsDetailBinding

class TipsDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTipsDetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTipsDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.activityTipsBackButton.setOnClickListener {
            onBackPressed()
        }

    }
}