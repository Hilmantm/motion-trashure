package id.kodesumsi.motiontrashure.feature.scan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.ActivityScanSuccessBinding

class ScanSuccessActivity : AppCompatActivity() {

    private lateinit var binding: ActivityScanSuccessBinding

    companion object {
        const val BARCODE_RESULT = "barcode_result"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScanSuccessBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val intent = intent
        binding.activityScanSuccessTrashbagId.text = "Trashbag ID ${intent.getStringExtra(BARCODE_RESULT)}"

        binding.activityScanSuccessBackBtn.setOnClickListener {
            finish()
        }

    }
}