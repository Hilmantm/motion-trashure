package id.kodesumsi.motiontrashure.feature.scan

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.budiyev.android.codescanner.*
import com.google.android.material.snackbar.Snackbar
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentScanBinding


class ScanFragment : Fragment() {

    private lateinit var binding: FragmentScanBinding
    private lateinit var codeScanner: CodeScanner

    companion object {
        const val REQUEST_CAMERA_PERMISSION_CODE = 101
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentScanBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        requestPermission()
        binding.fragmentScanToolbar.title = getString(R.string.scan)
        codeScanner = CodeScanner(requireContext(), binding.fragmentScanBarcodeScanner)
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false

        codeScanner.startPreview()

        codeScanner.decodeCallback = DecodeCallback {
            requireActivity().runOnUiThread {
                toSuccessScan(it.text)
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            requireActivity().runOnUiThread {
                Toast.makeText(requireContext(), "Camera initialization error: ${it.message}",
                    Toast.LENGTH_LONG).show()
            }
        }

        binding.fragmentScanInputBtn.setOnClickListener {
            if(binding.fragmentScanInputEdittext.text.toString() == "") {
                binding.fragmentScanInputEdittext.error = "Tidak boleh kosong atau arahkan kamera ke trashbag barcode"
            } else {
                binding.fragmentScanInputEdittext.setText("")
                toSuccessScan(binding.fragmentScanInputEdittext.text.toString())
            }
        }

    }

    private fun requestPermission() {
        when {
            ContextCompat.checkSelfPermission(
                requireContext(),
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED -> {
                // You can use the API that requires the permission.
            }
            shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA) -> {
                // In an educational UI, explain to the user why your app requires this
                // permission for a specific feature to behave as expected. In this UI,
                // include a "cancel" or "no thanks" button that allows the user to
                // continue using your app without granting the permission.
                showSnackbar()
            }
            else -> {
                // You can directly ask for the permission.
                // The registered ActivityResultCallback gets the result of this request.
                requestPermissions(arrayOf(android.Manifest.permission.CAMERA),
                    REQUEST_CAMERA_PERMISSION_CODE)
            }
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        when (requestCode) {
            REQUEST_CAMERA_PERMISSION_CODE -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() &&
                            grantResults[0] == PackageManager.PERMISSION_GRANTED)
                ) {
                    // Permission is granted. Continue the action or workflow
                    // in your app.
                } else {
                    showSnackbar()
                }
                return
            }
        }
    }

    private fun showSnackbar() {
        val snackbar = Snackbar.make(binding.fragmentScanContainer,
            "Izinkan kamera untuk fitur scan barcode trashbag",
            Snackbar.LENGTH_LONG)
        snackbar.setAction("Setting") {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri: Uri = Uri.fromParts("package", activity?.packageName, null)
            intent.data = uri
            startActivity(intent)
        }
        snackbar.show()
    }

    private fun toSuccessScan(trashbagId: String) {
        val toSuccessScan = Intent(context, ScanSuccessActivity::class.java).apply {
            putExtra(ScanSuccessActivity.BARCODE_RESULT, trashbagId)
        }
        startActivity(toSuccessScan)
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

}