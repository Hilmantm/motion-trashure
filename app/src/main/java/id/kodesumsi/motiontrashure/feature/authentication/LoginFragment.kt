package id.kodesumsi.motiontrashure.feature.authentication

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.feature.MainActivity
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.databinding.FragmentLoginBinding
import id.kodesumsi.motiontrashure.utils.GeneralUtility.changeTintColor
import id.kodesumsi.motiontrashure.viewmodel.AuthenticationViewModel

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var binding: FragmentLoginBinding

    private var TAG = LoginFragment::class.java.simpleName

    private val authenticationViewModel: AuthenticationViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // action to forget password fragment
        binding.fragmentLoginButtonLupaPassword.setOnClickListener {
            view.findNavController().navigate(R.id.action_loginFragment_to_forgetPasswordFragment)
        }

        // action to register fragment
        binding.fragmentLoginButtonDaftar.setOnClickListener {
            view.findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        binding.fragmentLoginButtonMasuk.setOnClickListener {
            val toMainActivity = Intent(context, MainActivity::class.java)
            context?.startActivity(toMainActivity)
            activity?.finish()
        }

        authenticationViewModel.seePasswordState(false)

        binding.fragmentLoginFieldPasswordSeeIcon.setOnClickListener {
            authenticationViewModel.seePasswordState(!authenticationViewModel.getSeePasswordState().value!!)
        }
        
        observeLiveData()
    }

    private fun observeLiveData() {
        authenticationViewModel.getSeePasswordState().observe(viewLifecycleOwner) {
            Log.d(TAG, "see password live data value: $it")
            if (it) {
                binding.fragmentLoginFieldPasswordSeeIcon.imageTintList = changeTintColor(requireContext(), R.color.colorPrimary)
                binding.fragmentLoginFieldPasswordEdittext.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                binding.fragmentLoginFieldPasswordSeeIcon.imageTintList = changeTintColor(requireContext(), R.color.colorAuthenticationGrey)
                binding.fragmentLoginFieldPasswordEdittext.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            binding.fragmentLoginFieldPasswordEdittext.setSelection(binding.fragmentLoginFieldPasswordEdittext.text.length)
        }
    }

}