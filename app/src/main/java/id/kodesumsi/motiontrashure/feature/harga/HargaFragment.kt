package id.kodesumsi.motiontrashure.feature.harga

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.HargaSampahAdapter
import id.kodesumsi.motiontrashure.databinding.FragmentHargaBinding
import id.kodesumsi.motiontrashure.model.HargaSampah

class HargaFragment : Fragment() {

    private lateinit var binding: FragmentHargaBinding

    private lateinit var hargaSampahAdapter: HargaSampahAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHargaBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.fragmentHargaSampahRecyclerview.layoutManager = LinearLayoutManager(context)
        hargaSampahAdapter = HargaSampahAdapter(initDummyHargaSampah())
        binding.fragmentHargaSampahRecyclerview.adapter = hargaSampahAdapter

        binding.fragmentHargaSampahToolbar.title = "Harga Sampah"
    }

    private fun initDummyHargaSampah(): List<HargaSampah> {
        val listOfHargaSampah: MutableList<HargaSampah> = mutableListOf()

        val hargaSampahSatu = HargaSampah(
            id = 1,
            jenisSampah = "Sampah plastik",
            hargaSampah = 3500,
            thumb = ContextCompat.getDrawable(requireContext(), R.drawable.harga_sampah_thumb_1)!!
        )
        listOfHargaSampah.add(hargaSampahSatu)

        val hargaSampahDua = HargaSampah(
            id = 1,
            jenisSampah = "Sampah kaleng",
            hargaSampah = 5000,
            thumb = ContextCompat.getDrawable(requireContext(), R.drawable.harga_sampah_thumb_2)!!
        )
        listOfHargaSampah.add(hargaSampahDua)

        val hargaSampahTiga = HargaSampah(
            id = 1,
            jenisSampah = "Sampah kardus",
            hargaSampah = 4000,
            thumb = ContextCompat.getDrawable(requireContext(), R.drawable.harga_sampah_thumb_3)!!
        )
        listOfHargaSampah.add(hargaSampahTiga)

        return listOfHargaSampah

    }

}