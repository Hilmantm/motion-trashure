package id.kodesumsi.motiontrashure.feature.beranda

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import dagger.hilt.android.AndroidEntryPoint
import id.kodesumsi.motiontrashure.R
import id.kodesumsi.motiontrashure.adapter.SetoranAdapter
import id.kodesumsi.motiontrashure.adapter.TipsAdapter
import id.kodesumsi.motiontrashure.databinding.FragmentBerandaBinding
import id.kodesumsi.motiontrashure.feature.notification.NotificationActivity
import id.kodesumsi.motiontrashure.feature.setting.SettingsActivity
import id.kodesumsi.motiontrashure.model.Setoran
import id.kodesumsi.motiontrashure.model.Tips
import id.kodesumsi.motiontrashure.utils.AxisFormatter

@AndroidEntryPoint
class BerandaFragment : Fragment() {

    private lateinit var binding: FragmentBerandaBinding

    private lateinit var setoranAdapter: SetoranAdapter

    private lateinit var tipsAdapter: TipsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBerandaBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        val listOfBarEntry: MutableList<BarEntry> = mutableListOf()
//        listOfBarEntry.add(BarEntry(0f, 10f))
//        listOfBarEntry.add(BarEntry(1f, 15f))
//        listOfBarEntry.add(BarEntry(2f, 5f))
//        listOfBarEntry.add(BarEntry(3f, 18f))
//        listOfBarEntry.add(BarEntry(4f, 25f))
//        listOfBarEntry.add(BarEntry(5f, 30f))
//        listOfBarEntry.add(BarEntry(6f, 35f))
//        listOfBarEntry.add(BarEntry(7f, 22f))
//        listOfBarEntry.add(BarEntry(8f, 32f))
//        listOfBarEntry.add(BarEntry(9f, 14f))
//        listOfBarEntry.add(BarEntry(10f, 34f))
//        listOfBarEntry.add(BarEntry(11f, 40f))
//
//        val xAxis = binding.fragmentBerandaChart.xAxis
//        xAxis.valueFormatter = AxisFormatter(AxisFormatter.MONTH)
//
//        val barDataSet = BarDataSet(listOfBarEntry, "Sampah Terkumpul")
//        val barData = BarData(barDataSet)
//        binding.fragmentBerandaChart.animateY(500)
//        binding.fragmentBerandaChart.offsetLeftAndRight(10)
//        binding.fragmentBerandaChart.data = barData

        childFragmentManager.beginTransaction().replace(R.id.fragment_beranda_chart, GraphicFragment()).commit()

        binding.fragmentBerandaSetoranLihatSemua.setOnClickListener {
            val toListOfSetoran = Intent(requireContext(), SetoranActivity::class.java)
            startActivity(toListOfSetoran)
        }

        binding.fragmentBerandaSetoranRv.layoutManager = LinearLayoutManager(context)
        setoranAdapter = SetoranAdapter(initDummySetoranData())
        binding.fragmentBerandaSetoranRv.adapter = setoranAdapter

        binding.fragmentBerandaTipsRv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        tipsAdapter = TipsAdapter(initDummyTipsData())
        binding.fragmentBerandaTipsRv.adapter = tipsAdapter

        binding.fragmentBerandaNotification.setOnClickListener {
            val toNotificationActivity = Intent(activity, NotificationActivity::class.java)
            startActivity(toNotificationActivity)
        }

        binding.fragmentBerandaSettings.setOnClickListener {
            val toSettingActivity = Intent(activity, SettingsActivity::class.java)
            startActivity(toSettingActivity)
        }
    }

    private fun initDummySetoranData(): List<Setoran> {
        val setorans: MutableList<Setoran> = mutableListOf()
        for (i in 1..5) {
            val setoran = Setoran(
                id = i,
                icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_baseline_delete_24)!!,
                trashBagId = "1020309940",
                date = "19 / 04 / 2020",
                status = if (i == 1) 0 else 1
            )
            setorans.add(setoran)
        }
        return setorans
    }

    private fun initDummyTipsData(): List<Tips> {
        val listOfTips: MutableList<Tips> = mutableListOf()
        val tips1 = Tips(
                id = 1,
                title = "Cara memilah sampah yang baik",
                desc = getString(R.string.fragment_tips_text),
                date = "12 April 2020",
                thumb = ContextCompat.getDrawable(requireContext(), R.drawable.tips_thumb_1)!!
        )
        listOfTips.add(tips1)

        val tips2 = Tips(
                id = 2,
                title = "Cara Menggunakan Trashbag ID",
                desc = getString(R.string.fragment_tips_text),
                date = "19 April 2020",
                thumb = ContextCompat.getDrawable(requireContext(), R.drawable.tips_thumb_2)!!
        )
        listOfTips.add(tips2)

        val tips3 = Tips(
                id = 3,
                title = "Cara Membuang Sampah Mantan",
                desc = getString(R.string.fragment_tips_text),
                date = "25 April 2020",
                thumb = ContextCompat.getDrawable(requireContext(), R.drawable.tips_thumb_3)!!
        )
        listOfTips.add(tips3)

        return listOfTips
    }
}