package id.kodesumsi.motiontrashure.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by hilma on 18/11/2020.
 */
@HiltAndroidApp
class BaseApplication: Application() {
}