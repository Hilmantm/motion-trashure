package id.thebrothers.siaptanggap.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import id.kodesumsi.motiontrashure.repository.NetworkRepository
import id.kodesumsi.motiontrashure.service.NetworkService

/**
 * Created by hilma on 18/11/2020.
 */
@InstallIn(ApplicationComponent::class)
@Module
object RepositoryModule {

    @Provides
    fun providesNetworkRepository(
        networkService: NetworkService
    ): NetworkRepository {
        return NetworkRepository(networkService)
    }

}